## Feedback is good for you

As software developers we are always exposed to feedback from other programmers. At the basis of the *agile software
development methodology* is the idea of a quick feedback loop, and this doesn't only refer to quick testing suite and
often demos of the stories that were implemented but also often code reviews. What this means is that your brilliant
code will be judged by others very often, when you are not pairing, at least every time you have a story completed, or
every few minutes when you are pairing. This could be nice, there will be many opportunities to have others see what a
smart cookie you are, or have someone else telling you that they don't like how you did something...

In this blog I try to describe what is **in my opinion** the difference between good feedback and bad feedback and how
to become a better feedback receiver.

### Giving Feedback

Giving good feedback is not easy.
When you read someone else's code you have to quickly grasp the context to really understand what is going on in there.
For the sake of making the next part sound saner let's name the feedback giver Bill and the feedback receiver Ralph.
So imagine Bill is an experienced programmer and Ralph is still a beginner.
 
Here are some examples of bad feedback in a code review:

1. "This looks horrible, that piece is confusing, I don't think that method is named properly. Let me know when it's in
a better state."
<br><br>
2. "This is wrong because it's not the  *<span style="text-decoration: underline;">framework name here<span>*  way of
doing it!"
<br><br>
3. "It looks good. Great Job!"

So let's analyze each example and see why that is bad feedback:

1. The problem here is that Bill just shared his opinion about a piece of code without giving better options for
approaching the problem. He also disregards the fact that the code was written by another human being. We developers
tend to forget the difference between talking to a computer or a real person. Humans are much more sensitive when it comes to harsh
comments about something that we did. Here Bill just ripped the code apart with no particular reasoning or explanations. This
feedback is not productive because Ralph was just informed that what was his best solution to a problem was 'horrible',
without any clue of what was wrong about it, or how to fix it.
<br><br>
2. Bill here told Ralph that he is not doing it the way it is usually expected to be done in this particular framework.
I always take feedback like this with a grain of salt. I am not saying that you shouldn't comply with the standards
expected in a framework. What I am saying is that you shouldn't do something the way the framework dictates when
something could be achieved a lot easier or cleaner doing it some other way. The problem here is that Bill didn't give a
reason why the framework way would be a better approach to the problem that the code is trying to solve.
<br><br>
3. Most people would prefer this type of feedback, Ralph feels like he did a great job, everybody should be happy. Or
should they? Let's think about it a bit better. So Bill finally gave some positive feedback. Ralph is happy whatever he
did was good. What did he do though that was good? This tells me that Bill either didn't examine the code well enough,
or he is just to lazy to tell Ralph what did he like about it. This is bad feedback because Ralph has no idea what he
he should continue doing and he is a bit suspicious whether Bill read the code or not.


Here are some examples of what good feedback would look like:

1. "I think this looks wrong because of (few reasons here)... One other way of doing this would be (one alternative way)..."
<br><br>
2. "This piece of code looks good because (a few reasons why it is good)..."

Now let's see why this is good feedback:

1. Bill has spotted a mistake that Ralph made, then gave him a few reasons why he thinks that the code is not the best it
could be. Notice the 'I think' part, Bill is letting Ralph know that this is his personal opinion and it should be
interpreted like one. Then Bill continues by giving an alternative approach to tackling this particular problem.
Is this good for Ralph? Yes it is. Now Ralph is aware of the mistakes that he had made and has a suggestion for the 
direction he should take to fix that. Ralph can research further to which solution to take or why what he did was wrong.

2. Bill is giving some positive feedback in a constructive way here. He specifically states which part of the code he
thinks is good and also gives a few reasons why he thinks that. This is constructive feedback for Ralph as well. Now he
knows what was an example of good code and maybe found out something new about why is that code good.

Overall giving poor feedback affects Bill as well. In this case Ralph is one of his team mates and if Ralph is doing bad,
then the entire team is doing bad. The good part about our industry is that we don't really compete against each other
for jobs, there is a place for everybody. Bill shouldn't be worried about Ralph taking his job. Bills interest is to have
everybody on his team as good as they can be. Giving good feedback is good for Bill because he will end up reviewing the
code for the same feature fewer times and features will be implemented faster.

### Receiving Feedback

If we want to work on a team we all have to learn to receive feedback. The era of cowboys in software development is
long gone. It's time to grow up and build some solid software, and this can only be achieved by working in a competent
team. For the following examples I will describe some fictitious characters.

When we just start programming we are very confident in our skills as developers and our ability to come up with
solutions to any problem. If something doesn't work it's not because the code that we wrote is not working, it's
because we didn't write enough code. Once everything is working properly we move a long. We end up writing a lot of
useless code. Because of our confidence and our recklessness we get feedback that we don't like most of the time. I
would argue that learning to receive feedback is one of the most important traits in software development. How can
someone grow as a developer if they can't accept someones critique towards their code.

So here are a few common stages that I have noticed in the process of learning how to get feedback

####The defensive stage:

- The programmers take code very personal. The code is like our baby. When we present it to somebody it's the greatest
thing that there is. And when we get some negative feedback about it, we fight back, *"Don't tell me how to write
my code!"* type of reply. Bill (the feedback giver) just questioned something that was the smartest thing we could come
up with. We become very defensive for what we intended to do and if Bill still thinks we are wrong then he doesn't
know what he is talking about. We hate getting negative feedback at this stage but we are still looking for positive
feedback. In this stage we learn to hack things together. We sometimes look for feedback on some super smart code
that is usually frowned upon (only the machine and the person that wrote it could understand it). And we slowly evolve
to the next stage.

####The over protective stage:

- After a while of getting a lot of negative feedback we become shy in showing our code to others just so they don't
rip it apart. We almost try to hide our code from others site. If we don't let it out nobody can hurt it.
We are like the over protective parent. Keeping the children inside is not the best for them but it's better than letting
them play basketball where they could fall and hurt themselves. Same with our code we would rather have bad code that
nobody sees than get feedback on our code. In this stage we are most likely in a team and we still have code reviews.
In this stage we would listen to any advice and take it to the heart even though some of them collide. We learn a lot in
this stage, we always try everything that is given to us, we become humble. And we slowly evolve to the next stage.

####The curious stage:

- After a while we start writing better code, and we start noticing colliding advices. In this stage we realize that
there are always several ways of doing something, some of them better than others, and some of them just a matter of
preference. We develop an eye for picking one solution that is the best fit for a given problem. Here we like getting
feedback on everything we do. We want to hear what other people would do in certain situations? We are curious what
other ways there are. This is also the stage where we acknowledge that there is always a way to improve our code we just
have to find it. And we slowly learn to give feedback to others.

These are just the three stages that I could split my evolution into.
Receiving feedback is not easy. Learning to cope with it and take advantage of it is the best tool one could have
to aid them in becoming a better programmer. In my opinion you can't be a good programmer if you are still in the
defensive stage. We have to understand that a different perspective on a problem adds a lot of value to our code. Also
when we give feedback we have to be mindful of what is useful feedback
The curious stage is where we all want to be. Once we get out of this stage we should probably stop writing code because
we just wound up in the **defensive stage** again.

